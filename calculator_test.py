'''
6. Lav et nyt python program, en basis lommeregner som kan `+ - * /`  
   Programmet skal kunne tage input fra bruger og skrive resultater i cli. Du må kun bruge funktioner til de fire regneoperationer.
7. Udvid din lommeregner til at logge input og resultater til en .txt fil, lav en funktion der spørger bruger efter et filnavn der skal gemmes til og lav også en funktion som du kan kalde hver gang et input eller resultat skal gemmes. Filen må ikke overskrives mens programmet kører.
8. Omskriv dit regne program så regne funktionerne er i en klasse for sig selv og fil funktionerne er i en anden klasse. Brug klasserne så du får samme funktionalitet som i punkt 5.
'''

from calculator_class import calculator
from calc_logger_class import calc_logger

log = calc_logger.create_calc_logger('calclog.log')

def program():
    calc = calculator()
    
    try:
        u_input = calc.user_input()
        calculation = ''
        operation_msg = ''

        if u_input['operation'] == 1:
            result = calc.add(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 2:
            result = calc.sub(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 3:
            result = calc.div(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 4:
            result = calc.mult(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        result = (f'{u_input["number1"]} {operation_msg} {u_input["number2"]} = {calculation}')
        print(result)
        log.info(result)
        print('restarting....')
        program()

    except ValueError as e:
        error_msg = 'only numbers and "q" is accepted as input, please try again'
        print(error_msg)
        log.error(error_msg)
        program()
    except ZeroDivisionError:
        print('cannot divide by zero, try again')
        program()

program()