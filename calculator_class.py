class calculator():

    n1 = 0.0
    n2 = 0.0
    result = {'calculation': 0.0, 'operation_msg': ''}
    u_input = {'number1': '', 'number2': '', 'operation': ''}

    def __init__(self):
        print('\nWelcome to the simple calculator (write q to quit)')

    def exit(self):
        print('goodbye...')
        exit(0)

    def user_input(self):
        print('enter the first number')
        number1 = input('> ')
        if number1 == 'q':
            self.exit()
        print('enter the second number')
        number2 = input('> ')
        if number2 == 'q':
            self.exit()
        print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
        operation = input('> ')
        if operation == 'q':
            self.exit()
        try:
            self.u_input['number1'] = float(number1)
            self.u_input['number2'] = float(number2)
            self.u_input['operation'] = int(operation)
        except ValueError:
            raise ValueError
        return self.u_input

    def add(self, num1, num2):
        self.n1 = num1
        self.n2 = num2
        try:
            self.result['calculation'] = float(self.n1 + self.n2)
            self.result['operation_msg'] = 'added with'
            return self.result
        except ValueError:
            raise ValueError

    def sub(self, num1, num2):
        self.n1 = num1
        self.n2 = num2
        try:
            self.result['calculation'] = float(self.n1 - self.n2)
            self.result['operation_msg'] = 'subtracted from'
            return self.result
        except ValueError:
            raise ValueError

    def div(self, num1, num2):
        self.n1 = num1
        self.n2 = num2
        try:
            self.result['calculation'] = float(self.n1 / self.n2)
            self.result['operation_msg'] = 'divided with'
            return self.result
        except ValueError:
            raise ValueError
        except ZeroDivisionError:
            raise ZeroDivisionError

    def mult(self, num1, num2):
        self.n1 = num1
        self.n2 = num2
        try:
            self.result['calculation'] = self.n1 * self.n2
            self.result['operation_msg'] = 'multiplied with'
            return self.result
        except ValueError:
            raise ValueError

    def test(self):
        print('testing calculator class methods')
        calc = calculator()

        try:
            calculation = ''
            operation_msg = ''
            self.u_input['number1'] = 5
            self.u_input['number2'] = 5
            self.u_input['operation'] = 1
            test_loop = 0

            while self.u_input['operation'] <= 4:
                if self.u_input['operation'] == 1:
                    result = calc.add(self.u_input['number1'], self.u_input['number2'])
                    operation_msg = result['operation_msg']
                    calculation = result['calculation']
                elif self.u_input['operation'] == 2:
                    result = calc.sub(self.u_input['number1'], self.u_input['number2'])
                    operation_msg = result['operation_msg']
                    calculation = result['calculation']
                elif self.u_input['operation'] == 3:
                    result = calc.div(self.u_input['number1'], self.u_input['number2'])
                    operation_msg = result['operation_msg']
                    calculation = result['calculation']
                elif self.u_input['operation'] == 4:
                    result = calc.mult(self.u_input['number1'], self.u_input['number2'])
                    operation_msg = result['operation_msg']
                    calculation = result['calculation']
                print(f'{self.u_input["number1"]} {operation_msg} {self.u_input["number2"]} = {calculation}')
                print('restarting....')
                self.u_input['operation'] += 1

        except ValueError:
            print('only numbers and "done" is accepted as input, please try again')
        except ZeroDivisionError:
            print('cannot divide by zero')
            exit(0)

def main():
    calc = calculator()
    calc.test()

if __name__ == '__main__':
    main()